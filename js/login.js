$(document).ready(function () {
$('#recpass').hide();
$('#mostrar_contrasena_log').click(function () {
    if ($('#mostrar_contrasena_log').is(':checked')) {
      $('#contrasenalog').attr('type', 'text');
    } else {
      $('#contrasenalog').attr('type', 'password');
    }
  });

  $('#mostrar_contrasena_recp').click(function () {
    if ($('#mostrar_contrasena_recp').is(':checked')) {
      $('#contrasenarecp').attr('type', 'text');
    } else {
      $('#contrasenarecp').attr('type', 'password');
    }
  });

  $("#recpassword").on("click", function(){
      $('#recpass').show();
      $('#log').hide();
  });
});